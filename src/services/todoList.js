const SERVER_URL = 'http://localhost:3000';

function getAll () {
  return fetch(`${SERVER_URL}/getAll`)
    .then(resp => {
      return resp.json();
    })
    .then(data => {
      return data;
    });
}

function addTodo (todo) {
  return fetch(`${SERVER_URL}/add?todo=${todo}`);
}

function deleteTodo (todo) {
  return fetch(`${SERVER_URL}/delete?todo=${todo}`);
}

export {
  getAll,
  addTodo,
  deleteTodo
}
;
